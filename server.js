var createError = require('http-errors');

var express = require('express');

var path = require('path'); 

var indexRouter = require('./routes/index');
var productRouter = require('./routes/products');
var orderRouter = require('./routes/orders');
var categoryRouter = require('./routes/categories');

var server = express();

// view engine setup
server.set('views', path.join(__dirname, 'views'));

server.set('view engine', 'ejs');

server.use(express.static(path.join(__dirname, 'public')));

// on line 48
// I ensure my server directs user to the index page of application
// ( index page as seen in the view folder is one of the front end files).
// The root page of application is the route without any name after it.
server.use('/', indexRouter);
server.use('/', productRouter);
server.use('/', orderRouter);
server.use('/', categoryRouter);

// catch 404 and forward to error handler
// on line 52 - 54 - I used my createError function here for my 404 error.
server.use(function(req, res, next) {
  next(createError(404));
});

// error handler
// I render error page anytime my server has errors. (error page as seen in my views folder is one of the front end files)  
server.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//every file you create, once you are done, you export the file.
// I export the server file on line 69
module.exports = server;
