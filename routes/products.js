var express = require('express');
var router = express.Router();

// Require product controller.
var productController = require('../controllers/productController');

// when users visit /product go to category controller
router.get('/product', productController.product_get);

module.exports= router;