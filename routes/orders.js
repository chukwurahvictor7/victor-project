var express = require('express');
var router = express.Router();

// Require order controller.
var orderController = require('../controllers/orderController');

// when users visit /order go to category controller
router.get('/order', orderController.order_get);

module.exports= router;