var Product = require('/products');
var Category = require('/categories');

module.exports = (sequelize, DataTypes) => {
    const Product_category = sequelize.define('Product_category', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        category_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        product_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        created_dt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('NOW()'),
        },
        last_modified_dt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('NOW()'),
        },
    });
    
    Product_category.belongsTo(Category, { foreignKey: 'category_id' });
    
    Product_category.hasMany(Product, { foreignKey: 'product_id' });
    
    return Product_category;
};