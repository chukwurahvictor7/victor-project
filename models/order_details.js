var Product = require('/products');
var Order = require('/orders');

module.exports = (sequelize, DataTypes) => {
    const Order_detail = sequelize.define('Order_detail', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        order_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        product_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        price: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        created_dt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('NOW()'),
        },
        last_modified_dt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('NOW()'),
        },
    });
    
    Order_detail.belongsTo(Order, { foreignKey: 'order_id' });
    
    Order_detail.belongsTo(Product, { foreignKey: 'product_id' });
    
    return Order_detail;
};