var Product = require('/products');
var Product_options = require('/product_options');

module.exports = (sequelize, DataTypes) => {
    const Option = sequelize.define('Option', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        option_name: {
            type: DataTypes.STRING,
            allowNull: true,
        },
        created_dt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('NOW()'),
        },
        last_modified_dt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('NOW()'),
        },
    });
    
    Option.hasMany(Product, { through: 'Product_options'});
    
    return Option;
};