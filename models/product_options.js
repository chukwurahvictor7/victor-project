var Product = require('/products');
var Option = require('/options');

module.exports = (sequelize, DataTypes) => {
    const Product_option = sequelize.define('Product_option', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        option_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        product_id: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        created_dt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('NOW()'),
        },
        last_modified_dt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('NOW()'),
        },
    });
    
    Product_option.belongsTo(Option, { foreignKey: 'option_id' });
    
    Product_option.belongsTo(Product, { foreignKey: 'product_id' });
    
    return Product_option;
};